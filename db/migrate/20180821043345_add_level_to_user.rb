class AddLevelToUser < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :instansi, :string
  	add_column :users, :minat, :string
  	add_column :users, :level, :string
  end
end
