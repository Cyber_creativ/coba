class SessionsController < ApplicationController
  def new
  end
  def create
  	user = User.find_by_email(params[:email])

  	if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to '/event/new'
    else
     	redirect_to '/welcome/index'
    end
  end
#   def save
#     user = User.find_by_email(params[:email])

#     if user && user.authenticate(params[:password])
# session[:user_id] = user.id
# redirect_to '/articles/new'
# else
#   redirect_to '/welcome/index'
#     end
#   end 
  def destroy
  	session[:user_id] = nil
  	redirect_to '/welcome/index'
  end
end
