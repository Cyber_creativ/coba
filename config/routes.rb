Rails.application.routes.draw do
  get 'events/new'
  get 'sessions/new'
  get 'articles/new'
  get 'users/profil'
  get 'welcome/index'
  root 'welcome#index'
  resources :users

  get '/login' =>'sessions#new'

  post '/login' =>'sessions#create'
  # post '/login' =>'sessions#save'
  get '/logout' =>'sessions#destroy'
  get '/signup' =>'users#new'
  post '/users'=>'users#create'
end
